export { default as Main } from './Main';
export { default as Hero } from './Hero';
export { default as Footer } from './Footer';
export { default as DarkModeSwitch } from './DarkModeSwitch';
export { default as CTA } from './CTA';
export { default as Container } from './Container';
