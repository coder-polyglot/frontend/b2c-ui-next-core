import NextDocument, {
  Html,
  Head,
  Main,
  NextScript,
} from 'next/document';
import { ColorModeScript } from '@chakra-ui/react';

export default class Document extends NextDocument {
  private html;

  constructor(props: any) {
    super(props);
    this.html = (
      <Html>
        <Head>
          <link rel="shortcut icon" href="/images/favicon.ico" />
        </Head>
        <body>
          {/* Make Color mode to persists when you refresh the page. */}
          <ColorModeScript />
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }

  render() {
    return this.html;
  }
}
