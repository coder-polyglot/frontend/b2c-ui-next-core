import { Text } from '@chakra-ui/react';
import Head from 'next/head';

import {
  Hero,
  Container,
  Main,
  DarkModeSwitch,
  Footer,
} from '../components';

const Index = () => (
  <Container height="100vh">
    <Head>
      <title>Coder Polyglot</title>
      <meta
        name="viewport"
        content="initial-scale=1.0, width=device-width"
      />
    </Head>

    <Hero title="Updating our Webapp for you" />
    <Main></Main>

    <DarkModeSwitch />
    <Footer>
      <Text>Made with ❤️ in Santiago Chile</Text>
    </Footer>
  </Container>
);

export default Index;
