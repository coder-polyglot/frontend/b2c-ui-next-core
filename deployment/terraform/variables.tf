variable "project" {}
variable "location" {}
variable "region" {}
variable "credentials" {}
variable "cluster_name" {}
variable "cluster_service_account_name" {}
variable "deploy_tag" {}