# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A SAMPLE CHART
# A chart repository is a location where packaged charts can be stored and shared. Define Bitnami Helm repository location,
# so Helm can install the nginx chart.
# ---------------------------------------------------------------------------------------------------------------------

resource "helm_release" "next-core" {
  chart = "../helm_chart"
  name  = "next-core"

  cleanup_on_fail = true

  set {
    name = "image.tag"
    value = var.deploy_tag != "" ? var.deploy_tag : "latest"
  }
}