# # configure kubectl with the credentials of the GKE cluster
# resource "null_resource" "configure_kubectl" {
#   provisioner "local-exec" {
#     command = "gcloud container clusters get-credentials ${var.cluster_name} --region ${var.region} --project ${var.project}"
#     # command = "gcloud beta container clusters get-credentials ${var.cluster_name} --region ${var.region} --project ${var.project}"

#     # Use environment variables to allow custom kubectl config paths
#     environment = {
#       KUBE_CONFIG_PATH = var.kubectl_config_path != "" ? var.kubectl_config_path : ""
#     }
#   }
# }